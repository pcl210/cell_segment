# Cell Segmentation

<br/> 
<div align=center><img src="figs/DALL·E 2023-06-29 22.43.03 - a robot investigates a Macque.png" width="500px"/></div>

## Overview

This repository contains code for training and deploying neural networks for cell counting in the histological images of macaque brain with neuroanatomical tracers. In this project, cell counting is formulated as patch-wise cell segmentation tasks.

<br/> 
<div align=center><<img src="figs/MethodOverview.png" width="500px"/>
<img src="figs/cell_counting.gif" width="500px"/></div>

## Installation

For Conda users, you can create a new Conda environment using

```
conda create -n cellsegment python=3.7
```

and then intall all the dependencies with

```
pip install -r requirements.txt
```

Note that we require the installation of `openslide`. In BMRC, we can load the module with command line `module load openslide/3.4.1`.

## Training a neural network for segmenting cells

### Cell segmentation dataset

Input images (in BMRC): The image paths are saved in `/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/cells_database/slides.csv`.

Manual annotation (in BMRC): The cell positions are saved in `/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/cells_database/cells.csv`.

Because we only have annotations in some parts of the images, we should generate the sampling regions before training. Here, we generate masks of the annotated hemisphere for the training process. The generation script is `Prepare_For_Training_Contour_Seg.ipynb`.


### Training a neural network

```console
Usage: python CellCounting_Training.py --model 0UNET1FORMER -epochnum EPOCHNUM [options]...

Training a 3D U-Net with 100 epochs: python CellCounting_Training.py --model 0 --epochnum 500 --val_interval 10 --batchsize 10

Training a SegFormer (pretrained with ImageNet) with 500 epochs: python CellCounting_Training.py --model 1 --epochnum 500 --val_interval 10 --batchsize 10

  --model               Choose different network architecture backbone. 0 for U-Net. Default: 0
  --epochnum            Number of total epochs to run. Default: 10
  --batchsize           Training batch size. Default: 16
  --patchsize           Training patch size. Default: 1024
  --val_interval        Have validation every several epoches. Default: 2
  --wce                 Weight for cell class in the cross entropy. Default: 100
  --epochbatch          Number of batches per epoch. Default: 1000
  --resume              To indicate if we want to resume a training process. Default: False
  --modelpath           The model ckpt to be resumed. Default: None
  --datapath            The folder contain all the tif images. Default: None
  --cellpath            The csv files containing cell positions. Default: None
  --slidepath           The csv files containing slides paths. Default: None
  --contourpath         The path to the masks for sampling (Prepare_For_Training_Contour_Seg.ipynb). Default: None

```

## Test an image

```console
Usage: python CellCounting_Inference.py --model 0UNET1FORMER --input INPUTFILE.tif --tempfolder ANYFOLDER --outputfolder OUTPUTFOLDER --modelpath MODEL.pth --threshold THRESHOLD [options]...

We provide pre-trained model, therefore the testing can be run out-of-box. Here is an example: python CellCounting_Inference.py --model 0 --input /well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr273FS_c03/mr273FS_c03_q90_tif/mr273FS_c03_s03n.tif --tempfolder /well/win-fmrib-analysis/users/gqu790/cell_segment/shell/model100ep_mr273FS_c03_s03n_t628/ --outputfolder /well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model100ep_t628/ --modelpath pretrained_models/model_0_epoch_200_wce_100.pth --threshold 628

  --model               Choose different network architecture backbone. 0 for U-Net. Default: 0
  --input               A tif file of quality 90 of while slide image. Default: None
  --tempfolder          Folder path to save some processing files. Default: None
  --outputfolder        Folder path to save output files. Default: None
  --modelpath           The pre-trained model path. Default: None
  --threshold           We only count as cell if the predicted area is larger than this threshold, e.g. lower threshold - > more sensitive. Default: 628  
  --cropsize            Determine the crop size for inference. Default: 100000
  --patchsize           Determine the patchsize for inference. Default: 1024
  --debug               Debug mode, save the middle results. Default: False
  --countonly           Run after debug mode, do the counting calculation with different thresholds. Default: False
```

The output results should be saved like this:

```
pred_model100ep_t628/
├── mr273FS_c03_s03n.png
├── mr273FS_c03_s03n.tif
├── mr273FS_c03_s03n_org.tif
├── mr273FS_c03_s03n.txt
└── ...
```

Where `*.png` is the thumbnail of the prediction, saved in very low resolution. `*.tif` is the illustration of the cell segmentation, saved in x8 lower resolution than the input images. `*.txt` is the predicted cell positions in the coordinate of the input image.

## Evaluation

If we have the ground truth annotation for the testing cases, we can evalutate our algorithm in terms of F1-measure, Recall etc. The evaluate script is shown in `Postprocess_Evaluation.ipynb`.ting cases, we can evalutate our algorithm in terms of F1-measure, Recall etc. The evaluate script is shown in `Postprocess_Evaluation.ipynb`.