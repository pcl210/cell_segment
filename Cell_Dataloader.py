#!/usr/bin/env python
# Copyright (c) 2023, Zeju Li
# All rights reserved.

import os
import time
from torch.utils.data import Dataset
from cucim import CuImage
import cv2
import numpy as np
from skimage import io
import pandas as pd
import matplotlib.pyplot as plt
from tifffile import imsave, imread
import random
from random import shuffle

# func
def setround(Img, X, Y, R):
    for xcor in range(X-R, X+R):
        for ycor in range(Y-R, Y+R):
            if (xcor-X)**2 + (ycor-Y)**2 < R**2:
                Img[xcor, ycor] = 1

def setlblmap(lblmap, xcor_lbl, ycor_lbl, cellx_fg, celly_fg, R, patchsize):
    for cellx_fgc, celly_fgc in zip(cellx_fg, celly_fg):
        X = cellx_fgc - (xcor_lbl - lblmap.shape[0] / 2)
        Y = celly_fgc - (ycor_lbl - lblmap.shape[1] / 2)
        
        if (R < X < lblmap.shape[0]-R) & (R < Y < lblmap.shape[1]-R):
            setround(lblmap, int(X), int(Y), R)
                
def getcrop(image_slide, xcenter, ycenter, patchsize, cellx_fg, celly_fg, R):
    
    perdist_x = np.random.randint(-patchsize/2, patchsize/2)
    perdist_y = np.random.randint(-patchsize/2, patchsize/2)
    
    xcor_lbl = xcenter + perdist_x
    ycor_lbl = ycenter + perdist_y
    
    #
    lblmap = np.zeros((patchsize + 4 * R, patchsize + 4 * R))
    setlblmap(lblmap, xcor_lbl, ycor_lbl, cellx_fg, celly_fg, R, patchsize)
    #

    lblregion = lblmap[int(lblmap.shape[0]/2-patchsize/2):int(lblmap.shape[0]/2+patchsize/2), 
                      int(lblmap.shape[0]/2-patchsize/2):int(lblmap.shape[0]/2+patchsize/2)]
    imgregion = image_slide.read_region(location=(xcor_lbl-int(patchsize/2), ycor_lbl-int(patchsize/2)), 
                                        size=(int(patchsize), int(patchsize)), level=0)
    
    return imgregion, lblregion.T


def getcells(image_slide, slideID, R, slidempp, cellread):
    cellx = []
    celly = []
    for cellid, xcor, ycor in zip(cellread.slide_id, cellread.cell_x, cellread.cell_y):
        if cellid == slideID:
            cellx.append(xcor / slidempp)
            celly.append(ycor / slidempp)
    return cellx, celly


class Cell_Dataset(Dataset):
    def __init__(self, patchsize = 512, R = 20, datasetlen = 10000, transform = None, target_transform = None, val = False, 
                datapath = '', 
                cellpath = '', 
                slidepath = '', 
                contourpath = ''
                ):
        '''
        Args:
        
        '''
        self.patchsize = patchsize
        self.val = val
        self.R = R
        self.datasetlen = datasetlen
        self.transform = transform
        self.target_transform = target_transform
        self.datapath = datapath
        self.contourpath = contourpath

        # initalize the sampling
        self.cellread = pd.read_csv(cellpath) # Sean's file
        self.slideread = pd.read_csv(slidepath) # Sean's file

        droplist = []
        ID = 0
        for cslide in self.slideread['slide_id']:
            if cslide not in list(self.cellread['slide_id']):
                droplist.append(ID)
            ID = ID + 1

        slideread_refined = self.slideread.drop(droplist)
        imglist = list(slideread_refined.image.str.replace('.jp2', '.tif', regex=False).str.replace('_jp2', '_q90_tif'))
        imglist_correct_res = imglist[:62]
        random.seed(0)
        shuffle(imglist_correct_res)
        # get the training/test split
        self.train_imglist = imglist_correct_res[:25]
        self.val_imglist = imglist_correct_res[25:28]


    def __len__(self):
        return self.datasetlen

    def __getitem__(self, idx):
        
        if self.val:
            imglist = self.val_imglist
        else:
            imglist = self.train_imglist
        
        # be careful here about the random process
        # I am debugging with pytorch 1.12.1, the random issue is fixed, the user should be careful with lower version
        sel = np.random.randint(0,len(imglist),1)
        slideID = imglist[sel[0]].split('/')[-1].split('.')[0]
        slidempp = self.slideread['mpp'][np.where(self.slideread['slide_id'] == slideID)[0][0]]
        image_slide = CuImage(self.datapath + imglist[sel[0]])
        
        # get the label map for this case
        cellx_fg, celly_fg = getcells(image_slide, slideID, self.R, slidempp, self.cellread)
        # run it off-line
        
        # roll a dice and decide whether where I sample the patches
        if np.random.normal() > 0:
            # sample from the cell
            selnum = np.random.randint(0,len(cellx_fg),1)
        
            cellx_sample = cellx_fg[selnum[0]]
            celly_sample = celly_fg[selnum[0]]
        else:
            # sample from the background
            chartregions = io.imread(self.contourpath + slideID + '_contour.png')
            nullregions_x, nullregions_y = np.where(chartregions.T > 0)

            selnum = np.random.randint(0,len(nullregions_x),1)
            cellx_sample = nullregions_x[selnum[0]] * 2 ** 6
            celly_sample = nullregions_y[selnum[0]] * 2 ** 6
        
        # get the sample
        xcenter = int(cellx_sample)
        ycenter = int(celly_sample)
        
        # threshold the sampling points
        xcenter = np.max((xcenter, self.patchsize))
        xcenter = np.min((xcenter, image_slide.metadata['cucim']['shape'][1]-self.patchsize))
        ycenter = np.max((ycenter, self.patchsize))
        ycenter = np.min((ycenter, image_slide.metadata['cucim']['shape'][0]-self.patchsize))     

        imgregion, lblregion = getcrop(image_slide, xcenter, ycenter, self.patchsize, cellx_fg, celly_fg, self.R)
        imgregion = np.array(imgregion)

        if self.transform:
            imgregion = self.transform(imgregion)
        if self.target_transform:
            lblregion = self.target_transform(lblregion)
        
        return imgregion, lblregion