#!/usr/bin/env python
# Copyright (c) 2023, Zeju Li
# All rights reserved.

import logging
import os
import sys
import tempfile
import argparse
import numpy as np
from glob import glob
import time

import torch
from torch import nn
from PIL import Image
from torch.utils.tensorboard import SummaryWriter
from Cell_Dataloader import Cell_Dataset

import monai
from monai.data import decollate_batch
from monai.inferers import sliding_window_inference
from monai.metrics import DiceMetric, LossMetric
from monai.losses import DiceCELoss, DiceLoss
from monai.transforms import AsDiscrete, Compose
import torchvision.transforms as transforms
from torch.utils.data import DataLoader

parser = argparse.ArgumentParser(description='PyTorch U-Net Cell Counting Training')
parser.add_argument('--model', default=0, type=int, help='Choose different network architecture backbone')
parser.add_argument('--epochnum', default=10, type=int, help='Number of total epochs to run')
parser.add_argument('--batchsize', default=16, type=int, help='Training batch size')
parser.add_argument('--patchsize', default=1024, type=int, help='Training patch size')
parser.add_argument('--val_interval', default=2, type=int, help='Have validation every several epoches')
parser.add_argument('--wce', default=100, type=int, help='Weight for cell class in the cross entropy')
parser.add_argument('--epochbatch', default=1000, type=int, help='Number of batches per epoch')
parser.add_argument('--exp', default='', type=str, help='The saved name for the experiments')

parser.add_argument('--resume', action='store_true', help='To indicate if we want to resume a training process')
parser.add_argument('--modelpath', default='', type=str, help='The model ckpt to be resumed')

parser.add_argument('--datapath', default='/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/', type=str, help='The folder contain all the tif images')
parser.add_argument('--cellpath', default='/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/cells_database/cells.csv', type=str, help='The csv files containing cell positions')
parser.add_argument('--slidepath', default='/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/cells_database/slides.csv', type=str, help='The csv files containing slides paths')
parser.add_argument('--contourpath', default='/well/win-fmrib-analysis/users/gqu790/cell_segment/thumb_s/', type=str, help='The path to the masks for sampling (Prepare_For_Training_Contour_Seg.ipynb)')

args = parser.parse_args()

def main():

    savefolder = f"./model_{args.model}_epoch_{args.epochnum}_wce_{args.wce}"

    if len(args.exp) > 0:
        savefolder = f"{savefolder}_{args.exp}"

    if not os.path.exists(savefolder):
        os.makedirs(savefolder)
    else:
        if not args.resume:
            # resume training
            args.resume = True
            args.modelpath = f"{savefolder}/ckpt.pth"

    monai.config.print_config()
    log_format = '%(asctime)s %(message)s'
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=log_format, datefmt='%m/%d %I:%M:%S %p')
    fh = logging.FileHandler(f"{savefolder}/log.txt")
    fh.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(fh)

    patchsize = args.patchsize
    batchsize = args.batchsize
    epochnum = args.epochnum
    val_interval = args.val_interval

    # define transforms for image and segmentation
    transform_train = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.96347285, 0.94422883, 0.90062644), (0.04938584, 0.06213848, 0.10648724)),
    ])

    transform_val = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.96347285, 0.94422883, 0.90062644), (0.04938584, 0.06213848, 0.10648724)),
    ])

    transform_train_target = transforms.Compose([
            transforms.ToTensor(),
            AsDiscrete(to_onehot=2)
        ])

    transform_val_target = transforms.Compose([
            transforms.ToTensor()
        ])

    # define array dataset, data loader
    cell_data_train = Cell_Dataset(patchsize = args.patchsize, datasetlen = args.epochbatch * args.batchsize, 
                                    transform = transform_train, target_transform = transform_train_target,
                                    datapath = args.datapath, cellpath = args.cellpath, slidepath = args.slidepath, contourpath = args.contourpath)
    cell_data_val = Cell_Dataset(patchsize = args.patchsize, datasetlen = 100 * args.batchsize, 
                                    transform = transform_val, target_transform = transform_val_target, val = True,
                                    datapath = args.datapath, cellpath = args.cellpath, slidepath = args.slidepath, contourpath = args.contourpath)
    # create a training and validation data loader
    train_loader = DataLoader(cell_data_train, batch_size=args.batchsize, shuffle=True, num_workers=6, pin_memory=True)
    val_loader = DataLoader(cell_data_val, batch_size=args.batchsize, shuffle=True, num_workers=6, pin_memory=True, worker_init_fn = lambda x: np.random.seed(x))

    dice_metric = DiceMetric(include_background=True, reduction="mean", get_not_nans=False)
    post_trans = AsDiscrete(argmax=True)
    # create UNet, DiceLoss and Adam optimizer
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if args.model == 0:
        # basic unet
        model = monai.networks.nets.UNet(
            spatial_dims=2,
            in_channels=3,
            out_channels=2,
            channels=(16, 32, 64, 128, 256),
            strides=(2, 2, 2, 2),
            num_res_units=2,
        ).to(device)
        def process_fn(outputs, win_data=None, importance_map_=None):
            return outputs, importance_map_
        def val_fn(outputs):
            return outputs

    if args.model == 1:
        # segformer
        from transformers import SegformerForSemanticSegmentation
        model = SegformerForSemanticSegmentation.from_pretrained('./pretrained_models/segformer-b0-finetuned-ade-512-512', ignore_mismatched_sizes=True, num_labels = 2).to(device)
        def process_fn(outputs, win_data=None, importance_map_=None):
            outputs = nn.functional.interpolate(outputs[0],
                                    size=[outputs[0].shape[-2] * 4, outputs[0].shape[-1] * 4], # (height, width)
                                    mode='bilinear',
                                    align_corners=False)
            return [outputs], importance_map_
        def val_fn(outputs):
            return outputs['logits']

    # check model size
    param_size = 0
    for param in model.parameters():
        param_size += param.nelement() * param.element_size()
    buffer_size = 0
    for buffer in model.buffers():
        buffer_size += buffer.nelement() * buffer.element_size()

    size_all_mb = (param_size + buffer_size) / 1024**2
    logging.info(args)
    logging.info('model size: {:.3f}MB'.format(size_all_mb))

    start_epoch = 0
    optimizer = torch.optim.Adam(model.parameters(), 1e-3)

    # start a typical PyTorch training
    best_metric = 0
    best_metric_epoch = -1
    epoch_loss_values = list()
    metric_values = list()
    writer = SummaryWriter(savefolder)

    if args.resume:
        checkpoint = torch.load(args.modelpath)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        start_epoch = checkpoint['epoch'] + 1
        best_metric = checkpoint['best_metric']
        best_metric_epoch = checkpoint['best_metric_epoch']   
        

    loss_function = monai.losses.DiceCELoss(include_background=False, softmax=True, lambda_dice = 1.0, lambda_ce = 1.0, ce_weight=torch.tensor((1,args.wce)).to(device))
    loss_function_Dice = monai.losses.DiceCELoss(include_background=False, softmax=True, lambda_dice = 1.0, lambda_ce = 0.0, ce_weight=torch.tensor((1,args.wce)).to(device))
    loss_function_CE = monai.losses.DiceCELoss(include_background=False, softmax=True, lambda_dice = 0.0, lambda_ce = 1.0, ce_weight=torch.tensor((1,args.wce)).to(device))
    
    for epoch in range(start_epoch, epochnum):
        logging.info("-" * 10)
        logging.info(f"epoch {epoch + 1}/{epochnum}")
        model.train()
        epoch_loss = 0
        step = 0
        for batch_data in train_loader:

            inputs, labels = batch_data[0].float().to(device), batch_data[1].to(device)

            optimizer.zero_grad()
            outputs = model(inputs)

            outputs, _ = process_fn(outputs)
            if isinstance(outputs, list):
                outputs = outputs[0]

            loss = loss_function(outputs, labels)
            loss_Dice = loss_function_Dice(outputs, labels)
            loss_CE = loss_function_CE(outputs, labels)
            loss.backward()
            
            optimizer.step()
            
            epoch_loss += loss.item()
            epoch_len = len(cell_data_train) // train_loader.batch_size

            if step % 100 == 0:
                logging.info(f"{step}/{epoch_len}, train_loss: {loss.item():.4f}")

                # r = torch.cuda.memory_reserved(0) / 1024 / 1024 / 1024
                # a = torch.cuda.memory_allocated(0) / 1024 / 1024 / 1024
                # logging.info(f"{r} GB memory reserved, {a} GB memory being allocated")

            step += 1

            writer.add_scalar("train_loss", loss.item(), epoch_len * epoch + step)
            writer.add_scalar("train_loss_dice", loss_Dice.item(), epoch_len * epoch + step)
            writer.add_scalar("train_loss_wce", loss_CE.item(), epoch_len * epoch + step)

        epoch_loss /= step
        epoch_loss_values.append(epoch_loss)
        logging.info(f"epoch {epoch + 1} average loss: {epoch_loss:.4f}")

        # adjust learning rate
        lr = 1e-3 * (1 - epoch / epochnum)**0.9
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr

        modelsave_name = f"{savefolder}/ckpt.pth"
        torch.save(
            {'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_metric': best_metric,
            'best_metric_epoch': best_metric_epoch,
                }, modelsave_name)

        if (epoch + 1) % val_interval == 0:
            model.eval()
            with torch.no_grad():
                val_images = None
                val_labels = None
                val_outputs = None
                for val_data in val_loader:
                    val_images, val_labels = val_data[0].float().to(device), val_data[1].to(device)
                    roi_size = (patchsize, patchsize)
                    sw_batch_size = args.batchsize
                    val_outputs = sliding_window_inference(val_images, roi_size, sw_batch_size, model, process_fn=process_fn)

                    val_outputs = [post_trans(val_fn(i)) for i in decollate_batch(val_outputs)]
                    # compute metric for current iteration
                    dice_metric(y_pred=val_outputs, y=val_labels)
                # aggregate the final mean dice result
                metric = dice_metric.aggregate().item()
                # reset the status for next validation round
                dice_metric.reset()
                metric_values.append(metric)
                if metric > best_metric:
                    best_metric = metric
                    best_metric_epoch = epoch + 1
                    modelsave_name = f"{savefolder}/model_best.pth"
                    torch.save(
                        {
                        'model_state_dict': model.state_dict(),
                            }, modelsave_name)
                    logging.info("saved new best metric model")
                logging.info(
                    "current epoch: {} current mean dice: {:.4f} best mean dice: {:.4f} at epoch {}".format(
                        epoch + 1, metric, best_metric, best_metric_epoch
                    )
                )
                writer.add_scalar("val_mean_dice", metric, epoch + 1)

    logging.info(f"train completed, best_metric: {best_metric:.4f} at epoch: {best_metric_epoch}")
    writer.close()


if __name__ == "__main__":

    main()
