#!/usr/bin/env python
# Copyright (c) 2023, Zeju Li
# All rights reserved.

import os
import sys
import shutil
import argparse
import torch
import time
import cv2
from glob import glob
from cucim import CuImage
from skimage import io, color
import numpy as np
import pandas as pd
import nibabel as nb
from torch import nn
from itertools import repeat
import multiprocessing as mp
from tifffile import imwrite

import monai
from monai import config
from monai.data import ArrayDataset, decollate_batch, DataLoader
from monai.inferers import sliding_window_inference
from monai.networks.nets import UNet
from monai.transforms import Activations, Compose, LoadImage, SaveImage, NormalizeIntensity, ScaleIntensityRange


parser = argparse.ArgumentParser(description='PyTorch U-Net Cell Counting Inference')
parser.add_argument('--model', default=0, type=int, help='Choose different network architecture backbone')
parser.add_argument('--input', default='', type=str, help='A tif file of quality 90 of while slide image')
parser.add_argument('--tempfolder', default='', type=str, help='Folder path to save some processing files')
parser.add_argument('--outputfolder', default='', type=str, help='Folder path to save output files')
parser.add_argument('--modelpath', default='', type=str, help='The pre-trained model path')
parser.add_argument('--threshold', default=628, type=int, help='We only count as cell if the predicted area is larger than this threshold, e.g. lower threshold - > more sensitive')
parser.add_argument('--cropsize', default=10000, type=int, help='Determine the crop size for inference')
parser.add_argument('--patchsize', default=1024, type=int, help='Determine the patchsize for inference')
parser.add_argument('--debug', action='store_true', help='Debug mode, save the middle results')
parser.add_argument('--countonly', action='store_true', help='Run after debug mode, do the counting calculation with different thresholds')

parser.add_argument('--normalize', action='store_true', help='Normalize the image on the fly, useful if the test image looks different')

args = parser.parse_args()

################################## Functions to crop the whold slides ##################################

def getcrop_val(image_slide, xcenter, ycenter, cropsize):
    
    xcor_img = xcenter
    ycor_img = ycenter

    imgregion = image_slide.read_region(location=(xcor_img-int(cropsize/2), ycor_img-int(cropsize/2)), 
                                        size=(int(cropsize), int(cropsize)), level=0)
    return imgregion

################################## Functions to crop the whold slides ##################################

def createsquarelabelmap(image_slide, cells_x, cells_y, slideID, R):
    lblmap = np.zeros((int(image_slide.metadata['cucim']['shape'][1]), int(image_slide.metadata['cucim']['shape'][0])))
    for xcor, ycor in zip(cells_x, cells_y):
        setsquare(lblmap, int(xcor), int(ycor), R)
    return lblmap

def createbblabelmap(image_slide, cells_x, cells_y, slideID, R):
    lblmap = np.zeros((int(image_slide.metadata['cucim']['shape'][1]), int(image_slide.metadata['cucim']['shape'][0])))
    for xcor, ycor in zip(cells_x, cells_y):
        setbb(lblmap, int(xcor), int(ycor), R)
    return lblmap

def setsquare(Img, X, Y, R):
    Img[X-R : min(X+R, Img.shape[0]), Y-R : min(Y+R, Img.shape[1])] = 1

def setbb(Img, X, Y, R):
    R_out = R
    Img[X-R_out : min(X+R_out, Img.shape[0]), Y-R_out : min(Y+R_out, Img.shape[1])] = 1
    R_in = np.int32(R * 0.8)
    Img[X-R_in : min(X+R_in, Img.shape[0]), Y-R_in : min(Y+R_in, Img.shape[1])] = 0

def countallcell(sepath, imgname, thresholdp, tilecount, cellx_samples, celly_samples, cropsize):

    cells_x_pred = []
    cells_y_pred = []
    prob_pred = []
    
    segres_load = nb.load(f"{sepath}/{imgname}_{tilecount}_seg.nii.gz")        
    segres_loaddata = segres_load.get_fdata()[1, :, :] # in previous version, I need transpose here.

    segres_binary = (segres_loaddata > 0.5).astype(np.uint8)

    # new on 5 Jun, 2023 by Zeju
    # load the image, multiply with the brain mask
    
    # img_load = io.imread(f"{args.tempfolder}/{imgname}_{tilecount}.png")
    # brainmask = (img_load[:, :, 0] < 235) | (img_load[:, :, 1] < 235) | (img_load[:, :, 2] < 235) 
    # segres_binary = segres_binary * brainmask

    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(segres_binary)
    sizes = stats[:, -1]
    centroidsc = centroids[1:]
    cellpos = np.where(sizes[1:] > thresholdp)

    for cellpos_cnt in cellpos[0]:
        x_whole_img = centroidsc[cellpos_cnt, 0] + cellx_samples - int(cropsize / 2)
        y_whole_img = centroidsc[cellpos_cnt, 1] + celly_samples - int(cropsize / 2)
        cells_x_pred.append(x_whole_img)
        cells_y_pred.append(y_whole_img)

        # sum probability
        prob = (output == (cellpos_cnt + 1)) * segres_loaddata
        prob_pred.append(np.sum(prob))

    del segres_load
    del segres_loaddata
    del segres_binary

    return cells_x_pred, cells_y_pred, prob_pred

def main():

    #################################################################### Preprocessing the data ####################################################################

    ################################## Generate tile center ##################################
    
    probfolder = args.tempfolder
    if os.path.isdir(args.tempfolder) == False:
        os.mkdir(probfolder)

    tilecount = 0

    visualimg = args.input
    print('Processing ' + visualimg)

    image_slide = CuImage(visualimg)
    slideID = visualimg.split('/')[-1].split('.')[0]
    imgname = 'img_' + visualimg.split('/')[-1].split('.')[0]
    # step1, make the lblmap for this case, generate cellx containing cells

    # if, the image size is smaller than the cropsize, get smaller cropsize\
    lblshape = image_slide.metadata['cucim']['shape']
    if np.min((lblshape[0], lblshape[1])) // 2 < args.cropsize:
        cropsize = np.min((lblshape[0], lblshape[1])) // 2
    else:
        cropsize = args.cropsize

    cellx = list(range(int(cropsize/2), lblshape[1] - int(cropsize/2), cropsize))
    celly = list(range(int(cropsize/2), lblshape[0] - int(cropsize/2), cropsize))

    # note that now I extend the boundary, to get the corner cases.
    cellx.append(lblshape[1] - int(cropsize/2))
    celly.append(lblshape[0] - int(cropsize/2))

    if not args.countonly:

        ################################## Load the learned network parameters ##################################
        
        ################################## Define the network parameters ##################################

        print(f"Runing with GPU {torch.cuda.get_device_name(0)}")
        t = torch.cuda.get_device_properties(0).total_memory / 1024 / 1024 / 1024
        print(f"Total GPU memory {t} GB")

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        if args.model == 0:
            # basic unet
            model = monai.networks.nets.UNet(
                spatial_dims=2,
                in_channels=3,
                out_channels=2,
                channels=(16, 32, 64, 128, 256),
                strides=(2, 2, 2, 2),
                num_res_units=2,
            ).to(device)
            def process_fn(outputs, win_data=None, importance_map_=None):
                return outputs, importance_map_
            def val_fn(outputs):
                return outputs

        if args.model == 1:
            # segformer
            from transformers import SegformerForSemanticSegmentation
            model = SegformerForSemanticSegmentation.from_pretrained('../pretrained_models/segformer-b0-finetuned-ade-512-512', ignore_mismatched_sizes=True, num_labels = 2).to(device)
            def process_fn(outputs, win_data=None, importance_map_=None):
                outputs = nn.functional.interpolate(outputs[0],
                                        size=[outputs[0].shape[-2] * 4, outputs[0].shape[-1] * 4], # (height, width)
                                        mode='bilinear',
                                        align_corners=False)
                return [outputs], importance_map_
            def val_fn(outputs):
                return outputs['logits']

        config.print_config()

        # here is where I save the model
        checkpoint = torch.load(args.modelpath)
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()

        # define transforms for image and segmentation

        if args.normalize:
            levelnum = len(image_slide.metadata['cucim']['resolutions']['level_dimensions'])
            imgsizetoshow = image_slide.metadata['cucim']['resolutions']['level_dimensions'][levelnum-1]
            wholeimg = image_slide.read_region(location=(0, 0), size=imgsizetoshow, level=levelnum-1)
            imgall = np.array(wholeimg).reshape(-1,3)
            dmean = np.mean(imgall / 255, axis = (0))
            dval = np.std(imgall / 255, axis = (0))
            print(f"Using dmean as {dmean}, dval as {dval}")
        else:
            # paramters from the training datasets
            dmean = [0.96347285, 0.94422883, 0.90062644]
            dval = [0.04938584, 0.06213848, 0.10648724]

        imtrans = Compose([
            ScaleIntensityRange(a_min = 0., a_max = 255., b_min = 0., b_max = 1.),
            NormalizeIntensity(subtrahend = dmean, divisor = dval,
            channel_wise=True)])

        post_trans = Compose([Activations(softmax=True)])
        

        t_start = time.time()

        for cellx_sample in cellx:
            for celly_sample in celly:
            
                xcenter = int(cellx_sample)
                ycenter = int(celly_sample)

                imgregion = getcrop_val(image_slide, xcenter, ycenter, cropsize)
                
                imgregion = np.transpose(np.array(imgregion).astype(int), [2, 0, 1])
                imgregion = torch.from_numpy(imgregion)

                #################################################################### Make the prediction ####################################################################

                ################################## Do the inference here with slideing windows ##################################
                with torch.no_grad():
                    val_data = imtrans(imgregion)
                    val_data = val_data[None, :]

                    val_images = val_data.to(device)
                    # define sliding window size and batch size for windows inference
                    roi_size = (args.patchsize, args.patchsize)
                    sw_batch_size = 4
                    
                    val_outputs = sliding_window_inference(val_images, roi_size, sw_batch_size, model, process_fn=process_fn)
                    val_outputs = [post_trans(val_fn(i)) for i in decollate_batch(val_outputs)]

                    probsavename = f"{probfolder}/{imgname}_{tilecount}_seg.nii.gz"

                    print(f"Saving {probsavename}")
                    img = nb.Nifti1Image(np.array(val_outputs[0].cpu()), np.eye(4))  # Save axis for data (just identity)
                    img.to_filename(probsavename)  # Save as NiBabel file
                    
                
                tilecount = tilecount + 1
            
        t_stop = time.time()

        print(f"Time for inference: {t_stop - t_start}", flush=True)

    #################################################################### Fetch the whole prediction ####################################################################

    ################################## Initialize the file paths ##################################
    thumb_savepath = args.outputfolder
    if os.path.isdir(thumb_savepath) == False:
        os.mkdir(thumb_savepath)

    # prepare mp
    cellx_samples = []
    celly_samples = []
    for cellx_sample in cellx:
        for celly_sample in celly:
            cellx_samples.append(cellx_sample)
            celly_samples.append(celly_sample)
    tilecount = list(range(len(celly_samples)))

    # only count cell that larger than this
    # TODO: Could be refined to make the algorithm more sensitive
    thresholdp = args.threshold

    print('Starting to make tiles with multiple threads..')

    # Fetch the results with multi-thread
    mp_pool = mp.Pool(processes=6)
    with mp_pool as pool:
        results = pool.starmap(countallcell, zip(repeat(probfolder), repeat(imgname), repeat(thresholdp), tilecount, cellx_samples, celly_samples, repeat(cropsize)))

    mp_pool.terminate()
    mp_pool.join()

    print('Multiple threads finished..')

    cells_x_pred = []
    cells_y_pred = []
    prob_pred = []

    kl = 0
    for cells_xc, cells_yc, prob_predc in results:
        cells_x_pred = cells_x_pred + cells_xc
        cells_y_pred = cells_y_pred + cells_yc
        prob_pred = prob_pred + prob_predc

    if prob_pred != []:
        prob_pred = np.array(prob_pred)/(max(np.array(prob_pred)))
    print(len(cells_x_pred))
    
    ################################## save the predicion results ##################################

    savepath = thumb_savepath + visualimg.split('/')[-1][:-3] + 'txt'

    dat = np.array([cells_x_pred, cells_y_pred, prob_pred])
    dat = dat.T
    np.savetxt(savepath, dat, delimiter = ',')

    print('Saved predictions as ' + savepath)

    ################################## save as thumb ##################################

    savepath = thumb_savepath + visualimg.split('/')[-1][:-3] + 'png'

    lblmap = createsquarelabelmap(image_slide, cells_x_pred, cells_y_pred, slideID, 400)

    imgsizetoshow = image_slide.metadata['cucim']['resolutions']['level_dimensions'][-3]
    wholelabeltoshow = cv2.resize(lblmap, dsize=(imgsizetoshow[1], imgsizetoshow[0]), interpolation=cv2.INTER_NEAREST)
    wholelabeltoshow = wholelabeltoshow.transpose([1,0]).astype(int)
    wholeimg = image_slide.read_region(location=(0, 0), size=imgsizetoshow, level=len(image_slide.metadata['cucim']['resolutions']['level_dimensions'])-3)

    io.imsave(savepath, np.uint8(255 * color.label2rgb(wholelabeltoshow, wholeimg, alpha=0.5, bg_label=0, bg_color=None, saturation=1)))

    print('Saved thumb as ' + savepath)

    ################################## save as tiff ##################################

    savepath = thumb_savepath + visualimg.split('/')[-1][:-3] + 'tif'

    level_res = 3

    imgsizetoshow = image_slide.metadata['cucim']['resolutions']['level_dimensions'][level_res]
    wholeimg = image_slide.read_region(location=(0, 0), size=imgsizetoshow, level=level_res)

    print('Generating annotations')
    
    lblmap = createbblabelmap(image_slide, cells_x_pred, cells_y_pred, slideID, 10 * (2 ** level_res))
    wholelabeltoshow = cv2.resize(lblmap, dsize=(imgsizetoshow[1], imgsizetoshow[0]), interpolation=cv2.INTER_NEAREST)
    wholelabeltoshow = wholelabeltoshow.transpose([1,0]).astype(int)

    print('Generating annotated tiff')

    alpha = 0.5
    saveimage = np.array(wholeimg)
    saveimage[wholelabeltoshow == 1] = alpha * saveimage[wholelabeltoshow == 1] + (255 * (1-alpha), 0, 0)

    print('Saving annotated tiff as ' + savepath)

    quality = 90
    savepath_anno = thumb_savepath + visualimg.split('/')[-1][:-4] + '_anno.tif'
    imwrite(savepath_anno, np.uint8(saveimage), photometric='rgb', compression = ('jpeg', quality ), bigtiff=True, tile=(256, 256))

    savepath_org = thumb_savepath + visualimg.split('/')[-1][:-4] + '_org.tif'
    # also save the original image, preparing for the registration later steps.
    imwrite(savepath_org, np.uint8(np.array(wholeimg)), photometric='rgb', compression = ('jpeg', quality ), bigtiff=True, tile=(256, 256))

    print('Saved tiff as ' + savepath)

    if not args.debug:
        # optional: delete the temp folder
        shutil.rmtree(args.tempfolder)
        print('Temporal folder ' + args.tempfolder + ' deleted')


if __name__ == "__main__":
    
    main()
