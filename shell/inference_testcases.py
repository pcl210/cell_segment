#!/usr/bin/env python
import glob
import os
import numpy as np
import pandas as pd
import os.path as op

# test func
quality = 90

def getval():
    cellpath = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/cells_database/cells.csv'
    slidepath = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/cells_database/slides.csv'
    cellread = pd.read_csv(cellpath) # Sean's file
    slideread = pd.read_csv(slidepath) # Sean's file
    droplist = []
    ID = 0
    for cslide in slideread['slide_id']:
        if cslide not in list(cellread['slide_id']):
            droplist.append(ID)
        ID = ID + 1
    slideread_refined = slideread.drop(droplist)
    imglist = list(slideread_refined.image.str.replace('.jp2', '.tif', regex=False).str.replace('_jp2', '_q90_tif'))
    imglist_correct_res = imglist[:62]
    import random
    from random import shuffle
    random.seed(0)
    np.random.seed(0)
    shuffle(imglist_correct_res)
    tif_files = imglist_correct_res[28:]
    return tif_files

bin = '/well/win-fmrib-analysis/users/gqu790/conda/skylake/envs/cellsegment/bin/python'
cmd = bin + ' /well/win-fmrib-analysis/users/gqu790/cell_segment/CellCounting_Inference.py --input {infile} --tempfolder {tempfolder} --outputfolder {outputfolder} --modelpath {modelpath} --threshold {threshold} --model {model} --normalize\n'
cmd_wonorm = bin + ' /well/win-fmrib-analysis/users/gqu790/cell_segment/CellCounting_Inference.py --input {infile} --tempfolder {tempfolder} --outputfolder {outputfolder} --modelpath {modelpath} --threshold {threshold} --model {model}\n'

## model trained:
# 0. u-net (monal default model) 500 epoches
model0 = '/well/win-fmrib-analysis/users/gqu790/cell_segment/slurm_bash/ckpt_model_0_epoch_500_wce_100.pth'
# 1. segformer 500 epoches
model1 = '/well/win-fmrib-analysis/users/gqu790/cell_segment/slurm_bash/ckpt_model_1_epoch_500_wce_100.pth'
# 0. u-net (monal default model) 500 epoches, wce 500
model0_ = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pretrained_models/model_0_epoch_500_wce_500.pth'

####################################################################################################################################################
## input: 78 new cases
## model: 0
## output: /well/win-fmrib-analysis/users/gqu790/cell_segment/noisy_thumb_pred_newcases/

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mf276LY_BF/mf276LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_modelv0_newcases/'
threshold = 628
model = 0
modelpath = model0

with open('./inference_commands_newcases_model.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

####################################################################################################################################################
## input: 10 validation cases
## model: 0

outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_500ep_t628/'
modelpath = model0
threshold = 628
model = 0
tif_files = getval()
with open('./inference_commands_val_test_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/model100ep_' + slide_ID + '_t628/'
        fpr.write(cmd.format(infile='/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/' + f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

####################################################################################################################################################
## input: 10 validation cases
## model: 1

outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_500ep_t628/'
modelpath = model1
threshold = 628
model = 1
tif_files = getval()
with open('./inference_commands_val_test_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/model1_500ep_' + slide_ID + '_t628/'
        fpr.write(cmd.format(infile='/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/' + f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

####################################################################################################################################################
## input: 10 validation cases
## model: 1

outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_500ep_t1256/'
modelpath = model1
threshold = 1256
model = 1
tif_files = getval()
with open('./inference_commands_val_test_model1_t1256.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/model1_500ep_' + slide_ID + '_t628/'
        fpr.write(cmd.format(infile='/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/' + f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# print(f'fsl_sub -q gpu_short -r gpu:p100-sxm2-16gb:1 -R 128 -l logs -t ./inference_commands.txt')
print(f'fsl_sub -q gpu_short -r gpu:1 -R 128 -l logs -t ./inference_commands.txt')

# 1 ####################################################################################################################################################
## input: 78 new cases, mf276LY_BF
## model: 1
## output: /well/win-fmrib-analysis/users/gqu790/cell_segment/noisy_thumb_pred_newcases/

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mf276LY_BF/mf276LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_newcases/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_newcases_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))


# 2 ####################################################################################################################################################
## input: 77 new cases v2, mr243LY_BF
## model: 1
## output: /well/win-fmrib-analysis/users/gqu790/cell_segment/noisy_thumb_pred_newcases_v2/

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr243LY_BF/mr243LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_newcasesv2/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_newcasesv2_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 3 ####################################################################################################################################################
## input: mr254FR_c04
## model: 1

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr254FR_c04/mr254FR_c04_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_mr254FR/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_mr254FR_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 4 ####################################################################################################################################################
## input: mr257FR_c04
## model: 1

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr257FR_c04/mr257FR_c04_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_mr257FR/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_mr257FR_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 5 ####################################################################################################################################################
## input: mf179FR_BF
## model: 1

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mf179FR_BF/mf179FR_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_mf179FR/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_mf179FR_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 6 ####################################################################################################################################################
## input: mr273FS_BF
## model: 1

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr273FS_BF/mr273FS_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_mr273FS/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_mr273FS_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 7 ####################################################################################################################################################
## input: mn036LY_BF
## model: 1

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mn036LY_BF/mn036LY_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_mn036LY/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_mn036LY_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 8 ####################################################################################################################################################
## input: mr244LY_BF
## model: 1

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr244LY_BF/mr244LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model1_mr244LY/'
threshold = 628
model = 1
modelpath = model1

with open('./inference_commands_mr244LY_model1.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))


# Implementation from Nov, 2024
# Using U-Net, wce500, with threshold of 10.

# 1 ####################################################################################################################################################
## input: 78 new cases, mf276LY_BF
## model: 0
## output: /well/win-fmrib-analysis/users/gqu790/cell_segment/noisy_thumb_pred_newcases/

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mf276LY_BF/mf276LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mf276LY/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mf276LY_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))


# 2 ####################################################################################################################################################
## input: 77 new cases v2, mr243LY_BF
## model: 0
## output: /well/win-fmrib-analysis/users/gqu790/cell_segment/noisy_thumb_pred_newcases_v2/

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr243LY_BF/mr243LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mr243LY/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mr243LY_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 3 ####################################################################################################################################################
## input: mr254FR_c04
## model: 0

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr254FR_c04/mr254FR_c04_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mr254FR/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mr254FR_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 4 ####################################################################################################################################################
## input: mr257FR_c04
## model: 0

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr257FR_c04/mr257FR_c04_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mr257FR/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mr257FR_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 5 ####################################################################################################################################################
## input: mf179FR_BF
## model: 0

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mf179FR_BF/mf179FR_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mf179FR/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mf179FR_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 6 ####################################################################################################################################################
## input: mr273FS_BF
## model: 0

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr273FS_BF/mr273FS_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mr273FS/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mr273FS_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 7 ####################################################################################################################################################
## input: mn036LY_BF
## model: 0

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mn036LY_BF/mn036LY_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mn036LY/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mn036LY_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))

# 8 ####################################################################################################################################################
## input: mr244LY_BF
## model: 0

datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr244LY_BF/mr244LY_BF_q90_tif'
tif_files = glob.glob(f'{datadir}/*.tif')
outputfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/pred_model0_mr244LY/'
threshold = 628
model = 0
modelpath = model0_

with open('./inference_commands_mr244LY_model0.txt', 'w') as fpr:
    for f in tif_files:
        slide_ID = f.split('/')[-1].split('.')[0]
        tempfolder = '/well/win-fmrib-analysis/users/gqu790/cell_segment/shell/' + slide_ID + '/'
        fpr.write(cmd_wonorm.format(infile=f, tempfolder=tempfolder, outputfolder=outputfolder, modelpath=modelpath, threshold=threshold, model=model))
