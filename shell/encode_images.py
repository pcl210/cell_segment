#!/usr/bin/env python
import glob
import os
import os.path as op
import argparse

# Set quality level
quality = 90

# Set the path to the 'vips' binary
bin = '/well/win-fmrib-analysis/users/zhh707/conda/skylake/envs/vips/bin/vips'

# Define the command template
cmd = bin + ' tiffsave {infile} {outfile} --tile --pyramid --compression jpeg --Q {quality} --tile-width 256 --tile-height 256 --bigtiff\n'

# Set up argument parsing to accept datadir as an input argument
parser = argparse.ArgumentParser(description="Generate VIPS encoding commands")
parser.add_argument('datadir', type=str, help='Directory containing .jp2 files')
args = parser.parse_args()

# Get the directory path from the arguments
datadir = args.datadir
# datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mf276LY_BF/mf276LY_BF_jp2'
# datadir = '/well/win-fmrib-analysis/projects/win-mac-histo/hand_labelled/mr243LY_BF/mr243LY_BF_jp2'

# Find all jp2 files in the directory
jp2_files = glob.glob(f'{datadir}/*.jp2')
saving_txt = f"./encode_commands_{datadir.split('/')[-1]}.txt"

# Create and write the encoding commands to a text file
with open(saving_txt, 'w') as fpr:
    for f in jp2_files:
        tif_file = f.replace('jp2', 'tif')
        tif_file = tif_file.replace('_tif', f'_q{quality}_tif')

        # Create output directory if it doesn't exist
        if not op.exists(op.dirname(tif_file)):
            os.makedirs(op.dirname(tif_file))

        # Write the command to the file
        fpr.write(cmd.format(infile=f, outfile=tif_file, quality=quality))

# Print final command for submission
# print(f'fsl_sub -q short -R 128 -l logs -t ./encode_commands.txt')
print(f'fsl_sub -q short -R 200 -l logs -t ./encode_commands.txt')
